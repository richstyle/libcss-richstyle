
# What is RichStyle?

RichStyle is an HTML5/CSS3 framework for Markdown, ePub, and clean HTML files.

As a Debian package, it adds a template file called `Markdown.md` to your file manager
(Dolphin/Nautilus/Caja/Nemo…), to help you:

* create a markdown file with basic YAML metadata fields,
* and view it as a styled HTML file in any web browser (under development).

It enhances your experience with any software converts lightweight markup language (LML) file format, like Markdown, reStructuredText, MediaWiki, AsciiDoc, BBCode, Org-mode, PmWiki,
into output file formats, like ePub, HTML and PDF; software like Pandoc, AcsiiDoc, ReText, Formiko, Zim, or Sigil.

## Features

* Meets natural language requirements; basically: directions (LTR/RTL) and numeral systems.
* Complies the Unicode 10.0+ standard for icon addressing (code points).
* Ready for client-side fine printing.

## Compatibility

* Chromium/Chrome 69
* Firefox 68

## To Do

### i18n Issues

* Planning to _automatically_ generate i18n-related CSS rules from [CLDR](https://github.com/unicode-org/cldr).
* Waiting for [`lang` as a CSS media feature](https://github.com/w3c/csswg-drafts/issues/5465).
* I wish I could learn Japanese someday, in order to provide better support for East Asian languages.
* Waiting and waiting for Arabic language support in Ubuntu font family!

### Other Issues

* I'm trying to use Pandoc as a back-end software to display markdown files as a styled HTML file, but... check this:<br/>
[How to associate markdown files with a web browser](https://unix.stackexchange.com/questions/637597/).
* I need a script to build `richstyle.svgs.css` from SVG files automatically.
* I use `strong` tag as a heading block under `nav.toolbar > nav` and `aside > nav` tags, whilst `strong` is actually just a phrasing element. In this case, I think `nav.toolbar > nav` and `aside > nav` tags should be replaced with `panel` and `paneltitle` in the future.<br/>
See: https://bkardell.com/common-panel/index.src.html
* Where to store `/etc/richstyle/compare.awk` file?<br/>
It seems it's not the right place.<br/>
Seems like `/var/lib/dpkg/info/` doesn't work too.<br/>
`compare.awk` file is actually used by `postinst` and `prerm` files.
* Dark mode is under development.
* A Chromium bug: [CSS system-font values don't work](https://bugs.chromium.org/p/chromium/issues/detail?id=905605).

## Demo

* English: https://richstyle.org/?reviews/demo-web-en
* Arabic: https://richstyle.org/?reviews/demo-web-ar

## Home Page

[https://richstyle.org](https://richstyle.org)
